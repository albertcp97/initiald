﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Partida : MonoBehaviour
{ //Para poner el texot en la meta
    public GameObject Texto;

    //Meto en un array todos los checkpoits para controlar los estados del circuito
   
    public GameObject[] check;

    //Cambiar la rotacion de la felcha que guia al player
    public GameObject flecha;

   //en que cancion esta
    private int pos;

    //audio
    private new AudioSource audio;

    //variable para controlar las vueltas
    private int vueltas = 0;
    //en que check point esta el coche
    private int estado;

    //si puede contar una vuelta o no
    private bool vuelta = false;

    //musica
    public AudioClip[] playList;

    //texto del tiempo actual
    public GameObject tiempoAct;


    //texto del record
    public GameObject BestTiempo;
    //variables para tratar el timer
    private float tiempo;
    private float tb = 0;

    //scriptable object para guardar el tiempo record
    public Flecha tmp;

    // Start is called before the first frame update
    void Start()
    {
        //musica
        pos = 0;
        audio = GetComponent<AudioSource>();
        ponerCancion();

        //inicaimos check point y vueltas
        estado = 1;
        vueltas = 0;

        //permitimos que se pueda modificar la variable del record
        EditorUtility.SetDirty(tmp);

        tiempo = 0;

        //añadimos el record en pantalla
        BestTiempo.GetComponent<TextMesh>().text = tmp.getTime();
        tb = float.Parse(tmp.getTime());
        if (tb == 0)
        {//si no hay record, se pone un valor alto para poder actualizarlo
            tb = 1000;
        }




    }

    private void ponerCancion()
    {
        audio.clip = playList[pos];
        audio.Play();
        StopAllCoroutines();
       
    }

    // Update is called once per frame
    void Update()
    {
        if (!audio.isPlaying)
            nextSong();
        inputSong();

        //comprobamos en que estado esta la carrera segun los checkpoints
        switch (estado)
        {//si pasa por meta apuntara al siguiente objetivo. Mirara en que vuelta estamos, si es 3 finaliza la carrera y guarda record, sino miramos si se puede actualizar las vueltas y el tiempo
            case 0:
                Vector3 t = new Vector3(check[0].transform.position.x, check[0].transform.position.y, check[0].transform.position.z);
                flecha.transform.LookAt(t);
                Debug.Log(vueltas);
                if (vueltas < 3)
                {
                    if (!vuelta)
                    {
                        vueltas++;
                        estado = 1;
                        if (vueltas == 2)
                        {
                            Texto.GetComponent<TextMesh>().text = vueltas + "/3";
                            } else Texto.GetComponent<TextMesh>().text = vueltas + "/3";
                        
                        vuelta = true;
                        if (vueltas > 0)
                        {
                           
                            //comprobamos si se rompe el record
                            if (tiempo<tb)
                            {
                                BestTiempo.GetComponent<TextMesh>().text = tiempoAct.GetComponent<TextMesh>().text;
                                tb = float.Parse(tiempoAct.GetComponent<TextMesh>().text);
                            }
                            //reiniciamos timer
                            tiempo = 0;

                        }
                    }
                }
                else
                { float f = float.Parse(tiempoAct.GetComponent<TextMesh>().text);
                    Debug.Log(f);
                    Debug.Log(tb);
                    if (f < tb)
                    {
                        BestTiempo.GetComponent<TextMesh>().text = tiempoAct.GetComponent<TextMesh>().text;
                        tb = f;
                    }
                    //guardamos el record
                    tmp.SetTime(tb.ToString());
                    //pasamos a la escena de victoria
                    SceneManager.LoadScene("Win");
                }
                
                  
                
                
                break;
                //todos los checkpoints restantes
            case 1:
                {
                    flecha.transform.rotation = Quaternion.Slerp(flecha.transform.rotation,
                   Quaternion.LookRotation(check[1].transform.position - flecha.transform.position), 6 * Time.deltaTime);
                   
                }
                break;

            case 2:
                flecha.transform.rotation = Quaternion.Slerp(flecha.transform.rotation,
                 Quaternion.LookRotation(check[2].transform.position - flecha.transform.position), 6 * Time.deltaTime);
                break;

            case 3:
                flecha.transform.rotation = Quaternion.Slerp(flecha.transform.rotation,
                 Quaternion.LookRotation(check[3].transform.position - flecha.transform.position), 6 * Time.deltaTime);
                break;

            case 4:
                flecha.transform.rotation = Quaternion.Slerp(flecha.transform.rotation,
                 Quaternion.LookRotation(check[4].transform.position - flecha.transform.position), 6 * Time.deltaTime);
                break;

            case 5:
                flecha.transform.rotation = Quaternion.Slerp(flecha.transform.rotation,
                 Quaternion.LookRotation(check[5].transform.position - flecha.transform.position), 6 * Time.deltaTime);
                break;

            case 6:
                flecha.transform.rotation = Quaternion.Slerp(flecha.transform.rotation,
                 Quaternion.LookRotation(check[6].transform.position - flecha.transform.position), 6 * Time.deltaTime);
                break;

            case 7:
                flecha.transform.rotation = Quaternion.Slerp(flecha.transform.rotation,
                 Quaternion.LookRotation(check[7].transform.position - flecha.transform.position), 6 * Time.deltaTime);
                break;
                //actualizamos para que se pueda guardar datos y sumar vuelta
            case 8:
                flecha.transform.rotation = Quaternion.Slerp(flecha.transform.rotation,
                 Quaternion.LookRotation(check[8].transform.position - flecha.transform.position), 6 * Time.deltaTime);
               
                break;
        }
        //si te quedas estancado puedes reiniciar partida
        if (Input.GetKey(KeyCode.R))
        {
            SceneManager.LoadScene("SampleScene");
        }

        //actualizamos timer en tiempo real
        tiempo += Time.deltaTime;
        //Debug.Log(tiempo);

        tiempoAct.GetComponent<TextMesh>().text = tiempo.ToString("f3");
       

    }

    private void inputSong()
    {

        if (Input.GetKeyDown("n"))
        {
            nextSong();
        }
        else if (Input.GetKeyDown("b"))
        {
            backSong();
        }
    }

    private void backSong()
    {
        pos--;
        if (pos < 0)
            pos = playList.Length - 1;
        ponerCancion();
    }

    private void nextSong()
    {
        pos++;
        if (pos >= playList.Length)
            pos = 0;
        ponerCancion();
    }


    //incrementar estado de los checkpoints
    public void incrementar()
    {
        if (estado !=9)
        {
            estado++;
        }
        else
        {
            vuelta = false;
            estado = 0;
        }
        
    }
}


