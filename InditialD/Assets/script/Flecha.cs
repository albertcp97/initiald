﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


[CreateAssetMenu]
public class Flecha : ScriptableObject
{
    //Guardar el valor del record

    // Start is called before the first frame update
    public string bestTime;

    public string getTime()
    {
        return bestTime;
    }
    public void SetTime(string time)
    {
         this.bestTime=time;
    }
}
